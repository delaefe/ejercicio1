package com.example.ej;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.Button;
import android.widget.Toast;

import com.example.ej.controllers.AuthController;
import com.google.android.material.textfield.TextInputLayout;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity {

    private Button btnLogin, btnRegister;
    private TextInputLayout tilUser, tilPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tilUser     = findViewById(R.id.activity_login_field_user);
        tilPassword = findViewById(R.id.activity_login_field_password);
        btnLogin    = findViewById(R.id.activity_login_btn_login);
        btnRegister = findViewById(R.id.activity_login_btn_register);

        btnLogin.setOnClickListener(view -> {
            String user     = tilUser.getEditText().getText().toString();
            String password = tilPassword.getEditText().getText().toString();

            boolean validUser       = !user.isEmpty();
            boolean validPassword   = !password.isEmpty();

            if(!validUser){
                tilUser.setError("Error usuario no indicado");
            }else {
                tilUser.setError(null);
                tilUser.setErrorEnabled(false);
            }

            if(!validPassword){
                tilPassword.setError("Error password no indicado");
            }else {
                tilPassword.setError(null);
                tilPassword.setErrorEnabled(false);
            }

            if(validPassword){
                AuthController controller = new AuthController(view.getContext());
                controller.login(user,password);
            }else if (validUser & !validPassword){
                Toast.makeText(view.getContext(), "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
            }
        });

        btnRegister.setOnClickListener(view -> {
            Toast.makeText(view.getContext(), "Logeando", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(view.getContext(), RegisterActivity.class);
            startActivity(i);
            finish();
        });
    }
}