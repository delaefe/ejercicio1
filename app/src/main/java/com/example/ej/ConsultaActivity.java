package com.example.ej;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import com.example.ej.controllers.AuthController;
import com.example.ej.controllers.EvaluationController;
import com.example.ej.models.Evaluation;
import com.example.ej.models.EvaluationMapper;
import com.example.ej.models.User;
import com.example.ej.ui.DatePickerFragment;
import com.example.ej.ui.EvaluationAdapter;
import com.example.ej.DetailActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ConsultaActivity extends AppCompatActivity {

    private Button               btnNewEvaluation, btnFiltrar,btnBack, btnClear;
    private TextInputLayout      tilFrom, tilTo;
    private ListView             lvEvaluaciones;
    private AuthController       authController;
    private EvaluationController evaluationController;
    final   String DATE_PATTERN = "yyyy-MM-dd";

    //private ArrayList<Evaluation> evaluationList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);

        evaluationController   = new EvaluationController(this);
        authController         = new AuthController(this);

        btnNewEvaluation= findViewById(R.id.activity_consulta_btn_new_evaluation);
        btnClear        = findViewById(R.id.activity_consulta_btn_clear);
        btnFiltrar      = findViewById(R.id.activity_consulta_btn_filter);
        btnBack         = findViewById(R.id.activity_consulta_btn_back);
        lvEvaluaciones  = findViewById(R.id.activity_consulta_lv_evaluations);
        tilFrom         = findViewById(R.id.activity_consulta_til_from);
        tilTo           = findViewById(R.id.activity_consulta_til_until);

        tilFrom.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilFrom, new Date());
        });
        tilTo.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilTo, new Date());
        });

        User user = authController.getUserSession();

        List<Evaluation> evaluationList = evaluationController.getAll();
        EvaluationAdapter adapter = new EvaluationAdapter(this, evaluationList);

        lvEvaluaciones.setAdapter(adapter);

        btnFiltrar.setOnClickListener(view -> {
            String fromStr  = tilFrom.getEditText().getText().toString();
            String toStr    = tilTo.getEditText().getText().toString();

            boolean validFrom   = !fromStr.isEmpty();
            boolean validTo     = !toStr.isEmpty();

            if(validFrom && validTo){
                SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
                try{
                    Date from    = dateFormatter.parse(fromStr);
                    Date to      = dateFormatter.parse(toStr);

                    List <Evaluation> evaluationRangeList = evaluationController.getRange(from,to);
                    EvaluationAdapter rangeAdapter = new EvaluationAdapter(this,evaluationRangeList);

                    lvEvaluaciones.setAdapter(rangeAdapter);
                    lvEvaluaciones.setOnItemClickListener(((adapterView, rangeView, index,id) ->{
                        Evaluation eval = evaluationRangeList.get(index);
                        Intent i = new Intent(rangeView.getContext(),DetailActivity.class);
                        i.putExtra("evaluation",eval);
                        rangeView.getContext().startActivity(i);
                    }));

                }catch(ParseException e){
                    e.printStackTrace();
                }
            }
        });

        btnClear.setOnClickListener( view -> {
            lvEvaluaciones.setAdapter(adapter);
            tilFrom.getEditText().setText("");
            tilTo.getEditText().setText("");
        });

        btnNewEvaluation.setOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), NewEvaluationActivity.class);
            view.getContext().startActivity(i);
        });

        btnBack.setOnClickListener( view ->{
            Intent i = new Intent(view.getContext(), MainActivity.class);
            view.getContext().startActivity(i);
        });

    }

    @Override
    protected void onResume(){
        super.onResume();
        List <Evaluation> evaluationList = evaluationController.getAll();
        EvaluationAdapter adapter        = new EvaluationAdapter(this,evaluationList);

        lvEvaluaciones.setAdapter(adapter);
        lvEvaluaciones.setOnItemClickListener(((adapterView, view, index,id) ->{
            Evaluation eval = evaluationList.get(index);
            Intent i = new Intent(view.getContext(),DetailActivity.class);
            i.putExtra("evaluation",eval);
            view.getContext().startActivity(i);
        }));
    }
}