package com.example.ej;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.ej.controllers.AuthController;
import com.example.ej.controllers.EvaluationController;
import com.example.ej.models.Evaluation;
import com.example.ej.models.User;
import com.example.ej.ui.DatePickerFragment;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NewEvaluationActivity extends AppCompatActivity {

    private TextInputLayout tilDate, tilWeight;
    private Button btnRegister, btnBack;
    final String DATE_PATTERN = "yyyy-MM-dd";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_evaluation);

        tilDate     =   findViewById(R.id.activity_new_evaluation_date);
        tilWeight   =   findViewById(R.id.activity_new_evaluation_weight);

        btnRegister =   findViewById(R.id.activity_new_evaluation_btn_register);
        btnBack     =   findViewById(R.id.activity_new_evaluation_btn_back);

        tilDate.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilDate, new Date());
        });

        btnRegister.setOnClickListener( view->{

            String evaluationDate = tilDate.getEditText().getText().toString();
            String evaluationWeight = tilWeight.getEditText().getText().toString();

            //TODO: validaciones de Strings

            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
            Date date = null;
            try {
                date = dateFormatter.parse(evaluationDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            AuthController authController = new AuthController(view.getContext());
            User user = authController.getUserSession();

            Log.v("Date: ", String.valueOf(date));
            Log.v("Weight: ", evaluationWeight);
            Log.v("Height: ", user.getHeight());
            Log.v("Id: ", String.valueOf(user.getId()));

            Evaluation evaluation = new Evaluation(date, evaluationWeight, user.getHeight(), user.getId());

            EvaluationController controller = new EvaluationController(view.getContext());
            controller.register(evaluation);

            Toast.makeText(view.getContext(),"Registrando evaluacion",Toast.LENGTH_SHORT).show();

        });
        btnBack.setOnClickListener( view -> {
            super.onBackPressed();
        });


    }
}