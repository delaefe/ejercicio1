/*  EVALUACION 1 - Requerimientos:
     Desarrollar una App que permita registrar a un usuario con usuario,
    nombre, apellido, fecha de nacimiento, estatura, y clave.

    Crear un login solicitando usuario y clave.
    Registrar evaluación física
    con fecha y peso, el cual automáticamente deberá obtener
    el IMC(Peso/Estatura^2).
    Eliminar mediciones, Consular progresos por rangos de
    tiempo, mostrando fecha de la medición, peso e IMC.

    Deberá crear icono tamaño 512x512px.

    Crear un Logo, con el nombre “Gym Fitness Club”.
    El logo podrá ser usado por usted donde cree que aplique y
    donde es requerido por la aplicación.

    Se pide:
    • Desarrollar interfaz de login. (listo)
    • Desarrollar interfaz de registro de usuario. (listo)
    • Desarrollar interfaz de registro de evaluaciones. (
    • Desarrollar interfaz para eliminar evaluaciones.
    • Desarrollar interfaz para consultar evaluaciones por rangos de tiempo.
 */

package com.example.ej;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.ej.controllers.AuthController;
import com.example.ej.models.User;

//ej3 = registro + login (buenos)
//-agregar validaciones de los campos
//-comprobar fechas
//-aplicar base de datos (dao)

public class MainActivity extends AppCompatActivity {

    private Button btnEvaluacion, btnConsulta, btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AuthController authController = new AuthController(this);
        User user = authController.getUserSession();

        btnEvaluacion   = findViewById(R.id.activity_main_btn_evaluacion);
        btnConsulta     = findViewById(R.id.activity_main_btn_consulta);
        btnLogout       = findViewById(R.id.activity_main_btn_logout);

        btnEvaluacion.setOnClickListener(view ->{
            Intent i = new Intent(view.getContext(), NewEvaluationActivity.class);
            startActivity(i);
            finish();
        });
        btnConsulta.setOnClickListener(view ->{
            Intent i = new Intent(view.getContext(), ConsultaActivity.class);
            startActivity(i);
            finish();
        });
        btnLogout.setOnClickListener(view->{
            authController.logout();
        });
    }


}