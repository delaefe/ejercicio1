package com.example.ej.models;

import java.util.Date;

public interface IEvaluation {
    long    getId();
    double  getWeight();
    Date    getDate();
    double  getBMI();
    long    getUserId();
    double  getHeight();
}