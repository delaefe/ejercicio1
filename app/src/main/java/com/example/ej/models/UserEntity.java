package com.example.ej.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName="users", indices = {@Index(value = "id", unique=true)})
public class UserEntity implements IUser {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="surname")
    private String surname;

    @ColumnInfo(name="username")
    private String username;

    @ColumnInfo(name="birthday")
    private Date birthday;

    @ColumnInfo(name="height")
    private String height;

    @ColumnInfo(name="password")
    private String password;

    public UserEntity(long id, String name, String surname, Date birthday, String height, String username, String password) {
        this.id         = id;
        this.name       = name;
        this.surname    = surname;
        this.username   = username;
        this.birthday   = birthday;
        this.height     = height;
        this.password   = password;
    }

    public long     getId(){       return id; }
    public String   getName(){     return name; }
    public String   getSurname(){  return surname; }
    public String   getUsername(){ return username; }
    public Date     getBirthday(){ return birthday; }
    public String   getHeight(){   return height; }
    public String   getPassword(){ return password; }
}
