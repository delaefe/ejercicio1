package com.example.ej.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Evaluation implements Serializable, IEvaluation {

    private Date    evaluationDate;
    private double  weight;
    private double  height;
    private double  BMI;
    private long    id;
    private long    userId;

    public Evaluation(Date evaluationDate, String weight, String height, long userId) {
        this.evaluationDate = evaluationDate;
        this.weight         = Double.parseDouble(weight);
        this.height         = Double.parseDouble(height);
        this.BMI            = this.weight/ (this.height*this.height);
        this.userId         = userId;
    }

    public String getEvaluationDateString(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(this.evaluationDate);
    }

    public Date     getEvaluationDate(){return this.evaluationDate; }
    public double   getWeight() {       return this.weight; }
    public Date     getDate() {         return this.evaluationDate; }
    public String   getWeightString(){  return Double.toString(this.weight); }
    public double   getBMI() {          return this.BMI; }
    public String   getBMIString(){     return Double.toString(this.BMI); }
    public long     getId() {           return this.id; }
    public long     getUserId(){        return this.userId;}
    public double   getHeight() {       return this.height; }
    public String   getIdString(){      return Long.toString(this.id); }

    public void     setId(long id) {    this.id = id; }

}
