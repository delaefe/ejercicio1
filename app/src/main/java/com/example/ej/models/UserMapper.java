package com.example.ej.models;

//* convierte de una clase a otra

public class UserMapper {

    private IUser user;

    public UserMapper(IUser user) {
        this.user = user;
    }

    public UserEntity toEntity(){
        return new UserEntity(
                this.user.getId(),
                this.user.getName(),
                this.user.getSurname(),
                this.user.getBirthday(),
                this.user.getHeight(),
                this.user.getUsername(),
                this.user.getPassword()
        );
    }

    public User toBase(){
        User userBase = new User(
                this.user.getName(),
                this.user.getSurname(),
                this.user.getBirthday(),
                this.user.getUsername(),
                this.user.getHeight()
        );
        userBase.setPassword(this.user.getPassword());
        userBase.setId(this.user.getId());
        return userBase;
    }
}
