package com.example.ej.models;

import android.util.Log;

import java.util.Date;

public class User implements IUser{

    private long    id;
    private String  name;
    private String  surname;
    private String  username;
    private Date    birthday;
    private String  height;
    private String  password;

    public User(String name, String surname, Date birthday, String username, String height){
        this.name       = name;
        this.surname    = surname;
        this.birthday   = birthday;
        this.username   = username;
        this.height     = height;
        Log.v("user ", this.name+" "+this.surname+" "+this.height+" "+this.birthday);
    }

    public void setPassword(String password) {  this.password = password; }
    public void setName(String name) {          this.name = name; }
    public void setId(long id){                 this.id=id; }
    public void setHeight(String height) {      this.height = height; }

    public long     getId(){        return this.id; }
    public String   getName() {     return this.name; }
    public String   getSurname() {  return this.surname; }
    public String   getUsername() { return this.username; }
    public Date     getBirthday() { return this.birthday; }
    public String   getHeight() {   return this.height; }
    public String   getPassword() { return this.password; }
}
