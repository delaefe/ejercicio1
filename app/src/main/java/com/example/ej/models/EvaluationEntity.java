package com.example.ej.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName="evaluations")
public class EvaluationEntity implements IEvaluation {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name="weight")
    private double weight;

    @ColumnInfo(name="date")
    private Date date;

    @ColumnInfo(name="BMI")
    private double BMI;

    @ColumnInfo(name="userId")
    private long userId;

    @ColumnInfo(name="height")
    private double height;

    public EvaluationEntity(long id, Date date, double weight, double height, double BMI, long userId){
        this.id     = id;
        this.date   = date;
        this.weight = weight;
        this.height = height;
        this.BMI    = BMI;
        this.userId = userId;
    }

    @Override
    public long     getId() {       return id; }
    @Override
    public double   getWeight() {   return weight; }
    @Override
    public Date     getDate() {     return date; }
    @Override
    public double   getBMI() {      return BMI; }
    @Override
    public long     getUserId(){    return this.userId;}
    @Override
    public double   getHeight() {   return this.height; }
}
