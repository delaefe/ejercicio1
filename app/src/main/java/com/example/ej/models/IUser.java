package com.example.ej.models;
import java.util.Date;

public interface IUser {
    long    getId();
    String  getName();
    String  getSurname();
    String  getUsername();
    Date    getBirthday();
    String  getHeight();
    String  getPassword();
}
