package com.example.ej.models;

public class EvaluationMapper {

    private IEvaluation evaluation;

    public EvaluationMapper(IEvaluation evaluation) {
        this.evaluation = evaluation;
    }

    public Evaluation toBase() {
        Evaluation baseEvaluation = new Evaluation(
                this.evaluation.getDate(),
                String.valueOf(this.evaluation.getWeight()),
                String.valueOf(this.evaluation.getHeight()),
                this.evaluation.getUserId()
        );
        baseEvaluation.setId(this.evaluation.getId());
        return baseEvaluation;
    }

    public EvaluationEntity toEntity(){
        return new EvaluationEntity(
                this.evaluation.getId(),
                this.evaluation.getDate(),
                this.evaluation.getWeight(),
                this.evaluation.getHeight(),
                this.evaluation.getBMI(),
                this.evaluation.getUserId()
        );

    }
}
