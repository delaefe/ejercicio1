package com.example.ej.utils;

import com.google.android.gms.common.util.NumberUtils;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//for string validation purposes
public class InputValidator {

    public static boolean validatePassword(String username, String password, TextInputLayout tilPassword){

        if(password.isEmpty()){
            tilPassword.setError("Debe indicar contraseña");
            return false;
        }
        if(password.length() < 6){
            tilPassword.setError("La contraseña debe tener más de 5 caracteres");
            return false;
        }
        if(password.equals(username)){
            tilPassword.setError("La contraseña no debe ser igual al nombre de usuario (duh..)");
            return false;
        }
        if(password.equals("123456")){
            tilPassword.setError("Esa contraseña la hackea hasta mi abuela...");
            return false;
        }

        return true;
    }

    public static boolean validateUsername(String username, TextInputLayout tilUsername){

        Pattern validChars = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = validChars.matcher(username);
        boolean checkChars = matcher.find();
        if(username.isEmpty()){
            tilUsername.setError("Debe indicar nombre de usuario");
            return false;
        }
        if(username.length() < 3){
            tilUsername.setError("El nombre de usuario debe tener más de 5 caracteres");
            return false;
        }
        if (checkChars){
            tilUsername.setError("El nombre de usuario debe ser alfanumérico (sin símbolos)");
            return false;
        }
        return true;
    }

    public static boolean validateHeight(String height, TextInputLayout tilHeight){

        double inputHeight;

        try{
            inputHeight = Double.parseDouble(height);
            if(inputHeight < .5){
                tilHeight.setError("Estatura muy baja (¿eres tú Frodo?)");
                return false;
            }
            if(inputHeight > 2.5){
                tilHeight.setError("Estatura demasiado alta ");
                return false;
            }
            return true;
        }catch(NumberFormatException ex){
            tilHeight.setError("Error (debe ingresar su estatura en decimales)");
            return false;
        }
    }


    public static boolean validateAge(int age, TextInputLayout tilAge){

        return true;
        /*
        if (age > 5){
            return true;
        }else {
            tilAge.setError("Debe tener más de 5 años de edad, mijo...");
            return false;
        }

         */
    }



}
