package com.example.ej.utils;

import androidx.room.TypeConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Converters {

    @TypeConverter
    public static Date fromTimestamp(Long value){
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date){
        return date == null ?  null : date.getTime();
    }

    @TypeConverter
    public static int birthdayToAge(String comingDate){
        long userAge = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        try {
            Date date1 = sdf.parse(sdf.format(cal.getTime()));
            Date date2 = sdf.parse(comingDate);
            userAge = ((date2.getTime() - date1.getTime())/86400000);

        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        return Math.abs((int) userAge);
    }
}
