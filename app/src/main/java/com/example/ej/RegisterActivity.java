package com.example.ej;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.ej.controllers.AuthController;
import com.example.ej.models.User;
import com.example.ej.ui.DatePickerFragment;
import com.example.ej.utils.Converters;
import com.example.ej.utils.InputValidator;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterActivity extends AppCompatActivity {

    private Button btnRegister;
    private TextInputLayout tilName,tilSurname,tilHeight,tilBirthday,tilUser,tilPass;
    final String DATE_PATTERN = "yyyy-MM-dd";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tilName     = findViewById(R.id.activity_register_nombre);
        tilSurname  = findViewById(R.id.activity_register_apellido);
        tilHeight   = findViewById(R.id.activity_register_height);
        tilUser     = findViewById(R.id.activity_register_username);
        tilBirthday = findViewById(R.id.activity_register_nacimiento);
        tilPass     = findViewById(R.id.activity_register_password);
        btnRegister = findViewById(R.id.activity_register_btn_register);

        tilBirthday.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilBirthday, new Date());
        });


        btnRegister.setOnClickListener(view -> {
            Log.v("registrando", tilName.getEditText().getText().toString() );

            String name     = tilName.getEditText().getText().toString();
            String surname  = tilSurname.getEditText().getText().toString();
            String username = tilUser.getEditText().getText().toString();
            String birthday = tilBirthday.getEditText().getText().toString();
            String height   = tilHeight.getEditText().getText().toString();
            String pass     = tilPass.getEditText().getText().toString();

            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
            Date bd = null;

            boolean validPassword   = InputValidator.validatePassword(username, pass, tilPass);
            boolean validName       = InputValidator.validateUsername(username,tilUser);
            boolean validHeight     = InputValidator.validateHeight(height,tilHeight);
            boolean validAge        = InputValidator.validateAge(Converters.birthdayToAge(birthday), tilBirthday );


            if(validPassword && validName && validHeight && validHeight && validAge){

                try {
                    bd = dateFormatter.parse(birthday);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                User newUser = new User(name, surname, bd, username, height);
                newUser.setPassword(pass);
                //newUser.setHeight(height);

                AuthController controller = new AuthController(view.getContext());
                controller.register(newUser);

            }else{
                Toast.makeText(view.getContext(),String.format("Hay errores en su formulario"),Toast.LENGTH_SHORT).show();
            }
        });
    }
}