package com.example.ej;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ej.controllers.EvaluationController;
import com.example.ej.models.Evaluation;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailActivity extends AppCompatActivity {

    private Button btnBack, btnDelete;
    private TextView tvId,tvFecha,tvPeso,tvBMI;

    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Evaluation evaluation = (Evaluation) getIntent().getSerializableExtra("evaluation");

        tvId        = findViewById(R.id.activity_detail_tv_id);
        tvFecha     = findViewById(R.id.activity_detail_fecha);
        tvPeso      = findViewById(R.id.activity_detail_peso);
        tvBMI       = findViewById(R.id.activity_detail_bmi);
        btnBack     = findViewById(R.id.activity_detail_btn_back);
        btnDelete   = findViewById(R.id.activity_detail_btn_delete);

        tvFecha.setText( new SimpleDateFormat("yyyy-mm-dd").format(evaluation.getEvaluationDate()));
        tvPeso.setText( String.format(" %.2f", evaluation.getWeight()) );
        tvBMI.setText(  String.format(" %.2f", evaluation.getBMI()) );
        tvId.setText(evaluation.getIdString());

        btnDelete.setOnClickListener(view->{
            EvaluationController controller = new EvaluationController(view.getContext());
            controller.delete(evaluation.getId());
        });
        btnBack.setOnClickListener(view ->{
            super.onBackPressed();
        });

    }



}
