package com.example.ej.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ej.R;
import com.example.ej.models.Evaluation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class EvaluationAdapter extends BaseAdapter {

    private Context ctx;
    private List<Evaluation> evList;

    public EvaluationAdapter(Context ctx, List<Evaluation> evList) {
        this.ctx    = ctx;
        this.evList = evList;
    }

    @Override
    public int getCount() {
        return evList.size();
    }

    @Override
    public Object getItem(int i) {
        return evList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return evList.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = LayoutInflater.from(ctx);

        view = inflater.inflate(R.layout.item_evaluation, null);

        Evaluation evaluation = evList.get(i);

        TextView tvDate   = view.findViewById(R.id.item_evaluation_date);
        TextView tvId     = view.findViewById(R.id.item_evaluation_id);
        TextView tvWeight = view.findViewById(R.id.item_evaluation_weight);
        TextView tvBMI    = view.findViewById(R.id.item_evaluation_BMI);

        tvDate.setText( String.format("Fecha: %s",evaluation.getEvaluationDateString() ) );
        tvId.setText( String.format("ID: %s",evaluation.getIdString()) );
        tvWeight.setText( String.format("Peso: %.2f",evaluation.getWeight()) );
        tvBMI.setText( String.format(   "IMC : %.2f",evaluation.getBMI  ()) );

        return view;
    }
}
