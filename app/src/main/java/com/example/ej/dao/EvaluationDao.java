package com.example.ej.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.ej.models.EvaluationEntity;

import java.util.Date;
import java.util.List;

@Dao
public interface EvaluationDao {

    @Query("SELECT ID, DATE, Weight, height, BMI, userId FROM EVALUATIONS where userid = :userId")
    List<EvaluationEntity> findAll(long userId);

    @Query("SELECT ID, DATE, Weight, height, BMI, userId FROM EVALUATIONS where userId = :userId and DATE between :from and :to")
    List<EvaluationEntity> findByRange(Date from, Date to, long userId);

    @Insert
    long insert (EvaluationEntity evaluation);

    @Query("DELETE FROM evaluations WHERE id = :id")
    void delete(long id);
}
