package com.example.ej.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.ej.models.UserEntity;

@Dao
public interface UserDao {
    @Query("SELECT id, username, name, surname, password, height FROM users WHERE username = :username LIMIT 1")
    UserEntity findByUsername (String username);

    @Insert
    long insert(UserEntity user);


}
