package com.example.ej.lib;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import androidx.room.Database;
import androidx.room.TypeConverters;

import com.example.ej.dao.EvaluationDao;
import com.example.ej.dao.UserDao;
import com.example.ej.models.EvaluationEntity;
import com.example.ej.models.UserEntity;
import com.example.ej.utils.Converters;


@Database(entities = {UserEntity.class, EvaluationEntity.class}, version=5)
@TypeConverters({Converters.class})
public abstract class EvaluationAppDatabase extends RoomDatabase {

    private static final String DB_NAME = "evaluation_app_db";
    private static EvaluationAppDatabase instance;

    public static synchronized EvaluationAppDatabase getInstance(Context ctx){
        if(instance==null){
            instance= Room.databaseBuilder(ctx.getApplicationContext(), EvaluationAppDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract UserDao userDao();

    public abstract EvaluationDao evaluationDao();
}
