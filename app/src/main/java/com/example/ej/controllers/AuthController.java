package com.example.ej.controllers;

import static android.media.MediaFormat.KEY_HEIGHT;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.example.ej.LoginActivity;
import com.example.ej.MainActivity;
import com.example.ej.dao.UserDao;
import com.example.ej.lib.BCrypt;
import com.example.ej.lib.EvaluationAppDatabase;
import com.example.ej.models.User;
import com.example.ej.models.UserEntity;
import com.example.ej.models.UserMapper;

import java.util.Date;

public class AuthController {

    private final String KEY_USER_ID = "userId";
    private final String KEY_USERNAME= "username";
    private final String KEY_NAME    = "userFirstName";
    private final String KEY_SURNAME = "userSurname";
    private final String KEY_HEIGHT  = "height";

    private UserDao userDao;
    private Context ctx;
    private SharedPreferences preferences;

    public AuthController(Context ctx){
        this.ctx = ctx;
        int PRIVATE_MODE = 0;
        String PREF_NAME="EvaluationAppPref";
        this.preferences = ctx.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        this.userDao= EvaluationAppDatabase.getInstance(ctx).userDao();
    }

    public User getUserSession() {
        long id         = preferences.getLong(KEY_USER_ID, 0);
        String name     = preferences.getString(KEY_NAME, "");
        String surname  = preferences.getString(KEY_SURNAME, "");
        String username = preferences.getString(KEY_USERNAME, "");
        String height   = preferences.getString(KEY_HEIGHT, "");

        User user = new User(name, surname, new Date(), username, height);
        user.setId(id);

        return user;
    }

    public void checkUserSession() {
        long id = preferences.getLong(KEY_USER_ID, 0);

        final int TIMEOUT = 6000;

        new Handler().postDelayed(() -> {
            if (id != 0) {
                Toast.makeText(ctx, "Bienvenido denuevo", Toast.LENGTH_LONG).show();
                Intent i = new Intent(ctx, MainActivity.class);
                ctx.startActivity(i);
            } else {
                Intent i = new Intent(ctx, LoginActivity.class);
                ctx.startActivity(i);
            }
            ((Activity) ctx).finish();
        }, TIMEOUT);

    }

    public void setUserSession(User user){

        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putLong( KEY_USER_ID, user.getId() );
        editor.putString(KEY_USERNAME, user.getUsername() );
        editor.putString(KEY_NAME, user.getName() );
        editor.putString(KEY_SURNAME, user.getSurname() );
        editor.putString(KEY_HEIGHT, user.getHeight() );

        editor.apply();
    }

    public void register(User user){

        String hashedPassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPassword(hashedPassword);

        UserEntity userEntity= new UserMapper(user).toEntity();

        userDao.insert(userEntity);

        Toast.makeText(ctx,String.format("Usuario %s registrado",user.getUsername()),Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ctx, LoginActivity.class);
        ctx.startActivity(i);
    }

    public void login(String username, String password){

        UserEntity userEntity = userDao.findByUsername(username);

        if(userEntity != null) { //user exists

            User user = new UserMapper(userEntity).toBase();

            if (BCrypt.checkpw(password, user.getPassword())) { //correct password
                setUserSession(user);
                Intent i = new Intent(ctx, MainActivity.class);
                ctx.startActivity(i);
                ((Activity) ctx).finish();
            } else {
                Toast.makeText(ctx, String.format("Contraseña incorrecta", username), Toast.LENGTH_SHORT).show();
            }

        }else{
            Toast.makeText(ctx, String.format("Usuario no registrado", username), Toast.LENGTH_SHORT).show();
        }
    }

    public void logout() {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.clear();
        editor.apply();
        Toast.makeText(ctx, "Cerrando Sesión", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(i);
        ((Activity) ctx).finish();
    }

}
