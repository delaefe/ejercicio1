package com.example.ej.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.example.ej.dao.EvaluationDao;
import com.example.ej.lib.EvaluationAppDatabase;
import com.example.ej.models.Evaluation;
import com.example.ej.models.EvaluationEntity;
import com.example.ej.models.EvaluationMapper;
import com.example.ej.models.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EvaluationController {

    private Context ctx;
    private EvaluationDao evaluationDao;


    public EvaluationController(Context ctx){
        this.ctx = ctx;
        this.evaluationDao=EvaluationAppDatabase.getInstance(ctx).evaluationDao();
    }

    public void register(Evaluation evaluation){
        EvaluationMapper mapper = new EvaluationMapper(evaluation);
        EvaluationEntity newEvaluation = mapper.toEntity();

        evaluationDao.insert(newEvaluation);

        ((Activity) ctx).onBackPressed();
    }

    public void delete(long id){
        DialogInterface.OnClickListener dialogClickListener = (dialog,which)->{
            switch(which){
                case DialogInterface.BUTTON_POSITIVE:
                    try{
                        evaluationDao.delete(id);
                        ((Activity) ctx).onBackPressed();
                        Toast.makeText(this.ctx,"eliminado", Toast.LENGTH_SHORT).show();
                    }catch(Error e){
                        e.printStackTrace();
                        Toast.makeText(this.ctx,"error", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this.ctx);
        builder.setMessage(String.format("¿Desea eliminar la evaluación?"))
                .setPositiveButton("si", dialogClickListener)
                .setNegativeButton("no", dialogClickListener)
                .show();
    }

    public List<Evaluation> getAll(){
        AuthController controller = new AuthController(ctx);
        User user = controller.getUserSession();

        List<EvaluationEntity> evaluationEntityList = evaluationDao.findAll(user.getId());
        List<Evaluation>  evaluationList = new ArrayList<>();

        for(EvaluationEntity evaluationEntity : evaluationEntityList){
            EvaluationMapper mapper = new EvaluationMapper(evaluationEntity);
            Evaluation evaluation = mapper.toBase();
            evaluationList.add(evaluation);
        }

        return evaluationList;
    }

    public List<Evaluation> getRange(Date from, Date to){
        AuthController controller = new AuthController(ctx);
        User user = controller.getUserSession();

        List<EvaluationEntity> evaluationEntityList = evaluationDao.findByRange(from,to,user.getId());
        List<Evaluation>  evaluationList = new ArrayList<>();

        for(EvaluationEntity evaluationEntity : evaluationEntityList){
            EvaluationMapper mapper = new EvaluationMapper(evaluationEntity);
            Evaluation evaluation = mapper.toBase();
            evaluationList.add(evaluation);
        }

        return evaluationList;
    }

}
